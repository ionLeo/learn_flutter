import 'package:flutter/material.dart';
import 'reuse/menu_drawer.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(title: Text('Hola')),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/city.jpeg'), fit: BoxFit.cover)),
        child: Center(
          child: Container(
            padding: EdgeInsets.all(10.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(2.0))),
            child: Text(
              "Hola mundo",
              style: TextStyle(color: Colors.red, fontSize: 30),
            ),
          ),
        ),
      ),
    );
  }
}
