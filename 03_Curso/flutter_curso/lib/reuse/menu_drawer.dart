import 'package:flutter/material.dart';
import '../weather_screen.dart';
import '../home_screen.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: buildMenuItems(context),
      ),
    );
  }

  List<Widget> buildMenuItems(BuildContext context) {
    final List<String> menuTitles = ['Home', 'Weather'];

    List<Widget> menuItems = [];
    menuItems.add(returnHeader());

    for (String title in menuTitles) {
      menuItems.add(returnItemMenu(title, context));
    }
    return menuItems;
  }

  DrawerHeader returnHeader() {
    return const DrawerHeader(
      decoration: BoxDecoration(
        color: Colors.blue,
      ),
      child: Text(
        "Flutter",
        style: TextStyle(color: Colors.white, fontSize: 28),
      ),
    );
  }

  ListTile returnItemMenu(String title, BuildContext context) {
    Widget screen = Container();

    return ListTile(
        title: Text(
          title,
          style: TextStyle(fontSize: 18),
        ),
        onTap: () {
          switch (title) {
            case 'Home':
              screen = HomeScreen();
              break;
            case 'Weather':
              screen = WeatherScreen();
              break;
          }
          Navigator.of(context).pop();
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => screen));
        });
  }
}
