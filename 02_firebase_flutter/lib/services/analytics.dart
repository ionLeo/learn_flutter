import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
//import 'package:flutter/material.dart';

class AnalyticsService {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();
  static final AnalyticsService _service = AnalyticsService._internal();

  factory AnalyticsService() => _service;
  AnalyticsService._internal();

  static FirebaseAnalytics get instance => _service._analytics;

  static FirebaseAnalyticsObserver get observer =>
      FirebaseAnalyticsObserver(analytics: _service._analytics);

  //Log all the logins with  the loginMethod that user chose to log in with.
  Future<void> logLogin() async {
    return _analytics.logLogin(loginMethod: 'email');
  }

//Set somo properties for our logged in
  Future<void> setUserProperties(
      {required String userId, required String userRole}) async {
    await _analytics.setUserId(userId);
    await _analytics.setUserProperty(name: 'user_role', value: userRole);
  }

  //Check aout if a basket for user is empty or not when they log out, Custom property
  Future<void> logLogoutPressed({
    bool isBasketEmpty = true,
  }) async {
    return _analytics.logEvent(
        name: 'logout_pressed', parameters: {'is_basket_empty': isBasketEmpty});
  }
}
