import 'package:firebase_analytics/observer.dart';
import 'package:wiredbrain/coffee_router.dart';
import 'package:wiredbrain/services/analytics.dart';
import '../get_theme.dart';
import '../screens/splash_screen.dart';
import 'package:flutter/material.dart';
//import 'package:firebase_analytics/firebase_analytics.dart';
//import 'package:firebase_analytics/observer.dart';
//import '../screens/menu.dart';

class CoffeeApp extends StatelessWidget {
  const CoffeeApp({Key? key}) : super(key: key);
  //To follow your user when the navigating between different screens
  static FirebaseAnalyticsObserver observer = AnalyticsService.observer;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      home: SplashScreen(),
      navigatorKey: CoffeeRouter.instance.navigatorKey,
      navigatorObservers: <NavigatorObserver>[observer],
      theme: getTheme(),
    );
  }
}
