import 'package:flutter/material.dart';

//StatelessWidget es un Widget que nunca cambia en tiempo de ejecución
class GlobeAppBeginner extends StatelessWidget {
  const GlobeAppBeginner({Key? key}) : super(key: key);

  @override
  //build es llamado cuando se empieza a dibujar la pantalla
  //context para interactuar con parent widgets u obtener el tamaño de la pantalla
  Widget build(BuildContext context) {
    //La palabre new esta implicita cada vez que llamos una clase
    return const MaterialApp(
      //A comoda lo que este dentro de el en el centro, pantalla u otro widget
      home: Center(
        //Child permite anidar widgets dentro de otros
        child: Text('Hello World'),
      ),
    );
  }
}
