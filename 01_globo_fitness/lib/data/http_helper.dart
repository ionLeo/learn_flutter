import 'package:globo_fitness/data/weather.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
//5124a9c2877c0b338aff7ac1d2337779 flutterApp

//https://api.openweathermap.org/data/2.5/
//weather?q=Monterrey&appid=217ddf1301fe89194d3e030b246f1d3a

class HttpHelper {
  final String authority = 'api.openweathermap.org';
  final String path = 'data/2.5/weather';
  final String apiKey = '217ddf1301fe89194d3e030b246f1d3a';

  Future<Weather> getWeather(String location) async {
    Map<String, dynamic> parameters = {'q': location, 'appid': apiKey};
    Uri url = Uri.https(authority, path, parameters);
    http.Response result = await http.get(url);
    Map<String, dynamic> data = json.decode(result.body);
    Weather weather = Weather.fromJson(data);

    return weather;
  }
}
