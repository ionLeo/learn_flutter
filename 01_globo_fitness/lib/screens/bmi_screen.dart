import 'package:flutter/material.dart';
import '../shared/menu_drawer.dart';
import '../shared/bottom_navigation.dart';

class BmiScreen extends StatefulWidget {
  const BmiScreen({Key? key}) : super(key: key);

  @override
  State<BmiScreen> createState() => _BmiScreenState();
}

class _BmiScreenState extends State<BmiScreen> {
  final TextEditingController txtHeight = TextEditingController();
  final TextEditingController txtWeight = TextEditingController();

  final double fontSize = 18;
  String result = '';
  bool isMetric = true;
  bool isImperial = false;
  double? height;
  double? weight;
  //Variable lazy
  late List<bool> isSelectd;
  String heightMessage = '';
  String weightMessage = '';

  @override
  void initState() {
    super.initState();

    isSelectd = [isMetric, isImperial];
  }

  @override
  Widget build(BuildContext context) {
    heightMessage =
        'Please insert your height in ' + ((isMetric) ? 'meters' : 'inches');
    weightMessage =
        'Please insert your weight in ' + ((isMetric) ? 'kilos' : 'pounds');

    return Scaffold(
        appBar: AppBar(
          title: Text('BMI Calculator'),
        ),
        bottomNavigationBar: BottomNav(),
        drawer: MenuDrawer(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              ToggleButtons(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Metric',
                      style: TextStyle(fontSize: fontSize),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      'Imperial',
                      style: TextStyle(fontSize: fontSize),
                    ),
                  )
                ],
                isSelected: isSelectd,
                onPressed: toggleMeasure,
              ),
              Padding(
                padding: const EdgeInsets.all(24.0),
                child: TextField(
                  controller: txtHeight,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(hintText: heightMessage),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(24.0),
                child: TextField(
                  controller: txtWeight,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(hintText: weightMessage),
                ),
              ),
              ElevatedButton(
                  onPressed: findBMI,
                  child: Text(
                    'Caluclate BMI',
                    style: TextStyle(fontSize: fontSize),
                  )),
              Text(
                result,
                style: TextStyle(fontSize: fontSize),
              )
            ],
          ),
        ));
  }

  void toggleMeasure(value) {
    switch (value) {
      case 0:
        isMetric = true;
        isImperial = false;
        break;
      case 1:
        isMetric = false;
        isImperial = true;
        break;
    }

    setState(() {
      isSelectd = [isMetric, isImperial];
    });
  }

  void findBMI() {
    double bmi = 0;
    double height = double.tryParse(txtHeight.text) ?? 0;
    double weight = double.tryParse(txtWeight.text) ?? 0;

    if (isMetric) {
      bmi = weight / (height * height);
    } else {
      bmi = weight * 703 / (height * height);
    }

    setState(() {
      //toStringAsFixed permite configurar la cantidad de decimales a mostrar
      result = 'Your BMI is ' + bmi.toStringAsFixed(2);
    });
  }
}

//Lifecycle events 
//initStat, build, dipose