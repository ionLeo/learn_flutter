import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import '../data/sp_helper.dart';
import '../data/session.dart';

class SessionPlatformScreen extends StatefulWidget {
  SessionPlatformScreen({Key? key}) : super(key: key);

  @override
  _SessionPlatformScreenState createState() => _SessionPlatformScreenState();
}

class _SessionPlatformScreenState extends State<SessionPlatformScreen> {
  List<Session> sessions = [];
  final TextEditingController txtDescription = TextEditingController();
  final TextEditingController txtDuration = TextEditingController();
  final SPHelper helper = SPHelper();

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      appBar: PlatformAppBar(
        title: PlatformText('Your Training Session'),
      ),
      body: ListView(
        children: getContent(),
      ),
    );
  }

  List<Widget> getContent() {
    List<Widget> titles = [];

    sessions.forEach((session) {
      titles.add(Dismissible(
        key: UniqueKey(),
        onDismissed: (_) {
          helper.deleteSession(session.id).then((value) => updateScreen());
        },
        child: ListTile(
          title: PlatformText(session.description),
          subtitle: PlatformText(
              '${session.date} - duration: ${session.duration} min'),
        ),
      ));
    });

    return titles;
  }

  void updateScreen() {
    sessions = helper.getSessions();
    setState(() {});
  }
}
