import 'package:flutter/material.dart';
import '../screens/bmi_screen.dart';
import '../screens/intro_screen.dart';
import '../screens/weather_screen.dart';
import '../screens/sessions_screen.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(children: buildMenuItems(context)),
    );
  }

  List<Widget> buildMenuItems(BuildContext context) {
    final List<String> menuTitles = [
      'Home',
      'BMI Calculator',
      'Weather',
      'Training'
    ];

    List<Widget> menuItems = [];

    //Agregamos el header
    menuItems.add(returHeader());

    //Agregamos las opciones del menu
    for (var element in menuTitles) {
      menuItems.add(returnItemMenu(element, context));
    }
    return menuItems;
  }

  DrawerHeader returHeader() {
    return const DrawerHeader(
      decoration: BoxDecoration(color: Colors.blueGrey),
      child: Text(
        'Globo Fitness',
        style: TextStyle(color: Colors.white, fontSize: 28),
      ),
    );
  }

  ListTile returnItemMenu(String title, BuildContext context) {
    Widget screen = Container();

    return ListTile(
      title: Text(
        title,
        style: TextStyle(fontSize: 18),
      ),
      onTap: () {
        switch (title) {
          case 'Home':
            screen = IntroScreen();
            break;
          case 'BMI Calculator':
            screen = BmiScreen();
            break;
          case 'Weather':
            screen = WeatherScreen();
            break;
          case 'Training':
            screen = SessionScreen();
            break;
          default:
            break;
        }
        Navigator.of(context).pop();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => screen));
      },
    );
  }
}
