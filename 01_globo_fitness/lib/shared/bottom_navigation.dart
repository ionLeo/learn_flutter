import 'package:flutter/material.dart';
import '../screens/bmi_screen.dart';
import '../screens/intro_screen.dart';

class BottomNav extends StatelessWidget {
  const BottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: barItems(context),
      onTap: (int index) {
        switch (index) {
          case 0:
            Navigator.pushNamed(context, '/');
            break;
          case 1:
            Navigator.pushNamed(context, '/bmi');
            break;
        }
      },
    );
  }

  List<BottomNavigationBarItem> barItems(BuildContext context) {
    final List<String> menuTitles = ['Home', 'BMI Calculator'];

    List<BottomNavigationBarItem> listBarItems = [];

    for (var title in menuTitles) {
      listBarItems.add(returnBarItem(title, context));
    }

    return listBarItems;
  }

  BottomNavigationBarItem returnBarItem(String title, BuildContext context) {
    return BottomNavigationBarItem(icon: returnIcon(title), label: title);
  }

  Icon returnIcon(String title) {
    switch (title) {
      case 'Home':
        return Icon(Icons.home);
      case 'BMI Calculator':
        return Icon(Icons.monitor_weight);
      default:
        return Icon(Icons.question_answer);
    }
  }
}
